Source: racket
Section: lisp
Priority: optional
Maintainer: David Bremner <bremner@debian.org>
Standards-Version: 4.1.1
Build-Depends: debhelper (>= 10),
               libcairo2-dev,
               libffi-dev,
               libfreetype6-dev,
               libgl1-mesa-dev | libgl-dev,
               libglu1-mesa-dev | libglu-dev,
               libjpeg-dev,
               liblz4-dev,
               libncurses-dev,
               libpango1.0-dev,
               libpng-dev,
               libssl-dev,
               libx11-dev,
               libxaw7-dev,
               libxft-dev,
               libxrender-dev,
               patchutils,
               sqlite3,
               xbitmaps,
               zlib1g-dev
Vcs-Git: https://salsa.debian.org/bremner/racket
Vcs-Browser: https://salsa.debian.org/bremner/racket
Homepage: https://www.racket-lang.org/

Package: racket
Architecture: any
Depends: racket-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: libgdk-pixbuf-2.0-0,
            libgdk-pixbuf-xlib-2.0-0,
            libglib2.0-0,
            libgtk2.0-0,
            libjpeg62-turbo,
            libpangocairo-1.0-0,
            libpng16-16,
            libssl3,
            racket-doc (>= ${source:Upstream-Version})
Breaks: racket-common (<<8.6~)
Replaces: racket-common (<<8.0~)
Description: extensible programming language in the Scheme family
 Racket (previously PLT Scheme) is a programming language suitable for
 scripting and application development, including GUIs and web
 services.
 .
 It supports the creation of new programming languages through a rich,
 expressive syntax system. Supplied languages include Typed Racket,
 ACL2, FrTime, and Lazy Racket, and R6RS Scheme.
 .
 Racket includes the DrRacket programming environment, a virtual
 machine with a just-in-time compiler, tools for creating stand-alone
 executables, the Racket web server, extensive libraries, and
 documentation for both beginners and experts.

Package: racket-common
Architecture: all
Breaks: racket (<< 8.6~)
Replaces: racket (<< 8.6~)
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: racket (>= ${source:Upstream-Version}),
            racket-doc (>= ${source:Upstream-Version}),
            sensible-utils
Description: extensible programming language in the Scheme family (shared files)
 This package includes the architecture independent files for Racket
 (but not the documentation, see racket-doc for that).

Package: racket-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: racket (>= ${source:Upstream-Version})
Enhances: racket
Description: extensible programming language in the Scheme family (documentation)
 This package includes all of the documentation for Racket.
